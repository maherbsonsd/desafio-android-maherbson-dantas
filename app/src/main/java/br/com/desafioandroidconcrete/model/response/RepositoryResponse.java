package br.com.desafioandroidconcrete.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroidconcrete.model.entity.Repository;

/**
 * Created by maher on 20/01/2018.
 */

public class RepositoryResponse implements Parcelable {

    @SerializedName("items")
    private List<Repository> repositoryList;

    protected RepositoryResponse(Parcel in) {
        if (in.readByte() == 0x01) {
            repositoryList = new ArrayList<Repository>();
            in.readList(repositoryList, Repository.class.getClassLoader());
        } else {
            repositoryList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (repositoryList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(repositoryList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<RepositoryResponse> CREATOR = new Parcelable.Creator<RepositoryResponse>() {
        @Override
        public RepositoryResponse createFromParcel(Parcel in) {
            return new RepositoryResponse(in);
        }

        @Override
        public RepositoryResponse[] newArray(int size) {
            return new RepositoryResponse[size];
        }
    };

    public List<Repository> getRepositoryList() {
        return repositoryList;
    }

    public void setRepositoryList(List<Repository> repositoryList) {
        this.repositoryList = repositoryList;
    }
}
