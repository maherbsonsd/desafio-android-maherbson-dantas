package br.com.desafioandroidconcrete.model.entity;

import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import br.com.desafioandroidconcrete.R;

/**
 * Created by maher on 20/01/2018.
 */

public class Repository implements Parcelable {

    @SerializedName("name")
    private String nameRepository;

    @SerializedName("description")
    private String descRepository;

    @SerializedName("watchers")
    private int numberStar;

    @SerializedName("forks")
    private int numberFork;

    @SerializedName("owner")
    private Owner owner;

    protected Repository(Parcel in) {
        nameRepository = in.readString();
        descRepository = in.readString();
        numberStar = in.readInt();
        numberFork = in.readInt();
        owner = (Owner) in.readValue(Owner.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameRepository);
        dest.writeString(descRepository);
        dest.writeInt(numberStar);
        dest.writeInt(numberFork);
        dest.writeValue(owner);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Repository> CREATOR = new Parcelable.Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public String getNameRepository() {
        return nameRepository;
    }

    public void setNameRepository(String nameRepository) {
        this.nameRepository = nameRepository;
    }

    public String getDescRepository() {
        return descRepository;
    }

    public void setDescRepository(String descRepository) {
        this.descRepository = descRepository;
    }

    public int getNumberStar() {
        return numberStar;
    }

    public void setNumberStar(int numberStar) {
        this.numberStar = numberStar;
    }

    public int getNumberFork() {
        return numberFork;
    }

    public void setNumberFork(int numberFork) {
        this.numberFork = numberFork;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String avatarUrl) {
        Picasso.with(view.getContext())
                .load(avatarUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(view);
    }
}
