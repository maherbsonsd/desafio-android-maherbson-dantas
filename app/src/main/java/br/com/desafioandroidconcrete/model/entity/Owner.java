package br.com.desafioandroidconcrete.model.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maher on 20/01/2018.
 */

public class Owner extends BaseObservable implements Parcelable {

    @SerializedName("login")
    private String name;

    @SerializedName("avatar_url")
    private String urlImage;

    protected Owner(Parcel in) {
        name = in.readString();
        urlImage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(urlImage);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Owner> CREATOR = new Parcelable.Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };

    @Bindable
    public String getName() {
        return name;
    }

    @Bindable
    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(br.com.desafioandroidconcrete.BR.name);
    }

    @Bindable
    public String getUrlImage() {
        return urlImage;
    }

    @Bindable
    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
        notifyPropertyChanged(br.com.desafioandroidconcrete.BR.urlImage);
    }

}
