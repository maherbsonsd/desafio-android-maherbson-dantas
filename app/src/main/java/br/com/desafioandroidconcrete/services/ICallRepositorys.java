package br.com.desafioandroidconcrete.services;

import java.util.List;

import br.com.desafioandroidconcrete.model.entity.PullRequest;
import br.com.desafioandroidconcrete.model.response.RepositoryResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by maher on 20/01/2018.
 */

public interface ICallRepositorys {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoryResponse> getRepository(@Query("page") int page);

    @GET("repos/{login}/{name}/pulls")
    Call<List<PullRequest>> getItemRepository(@Path("login") String creator, @Path("name") String repo);
}
