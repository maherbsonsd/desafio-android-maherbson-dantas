package br.com.desafioandroidconcrete.interfaces;

import java.util.List;

import br.com.desafioandroidconcrete.model.entity.PullRequest;
import br.com.desafioandroidconcrete.model.entity.Repository;

/**
 * Created by maher on 20/01/2018.
 */

public interface IRequestRepository {

    public interface IRequestRepo {
        void listRepository(List<Repository> repositoryList);
    }

    public interface IRequestPullRepo {
        void listPullRepository(List<PullRequest> pullRequestList);
    }

}
