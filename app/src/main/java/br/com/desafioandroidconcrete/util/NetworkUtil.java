package br.com.desafioandroidconcrete.util;

import android.content.Context;
import android.net.ConnectivityManager;

import br.com.desafioandroidconcrete.GlobalContext;

/**
 * Created by maher on 20/01/2018.
 */

public class NetworkUtil {

    public static boolean checkInternet() {
        ConnectivityManager cm = (ConnectivityManager) GlobalContext.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}
