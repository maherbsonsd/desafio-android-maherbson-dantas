package br.com.desafioandroidconcrete.request;

import br.com.desafioandroidconcrete.GlobalContext;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by maher on 20/01/2018.
 */

public class BaseRequest {

    private static String BASE_URL = "https://api.github.com/";
    private static Retrofit retrofit = null;
    private static int cacheSize = 10 * 1024 * 1024;

    public static Retrofit getInstanceRetrofit() {
        if (retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .cache(new Cache(GlobalContext.getAppContext().getCacheDir(), cacheSize))
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}