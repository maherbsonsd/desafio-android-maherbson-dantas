package br.com.desafioandroidconcrete.request.pullrequest;

import android.support.annotation.NonNull;

import java.util.List;

import br.com.desafioandroidconcrete.interfaces.IRequestRepository;
import br.com.desafioandroidconcrete.model.entity.PullRequest;
import br.com.desafioandroidconcrete.model.entity.Repository;
import br.com.desafioandroidconcrete.request.BaseRequest;
import br.com.desafioandroidconcrete.services.ICallRepositorys;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by maher on 20/01/2018.
 */

public class PullRequestRepo {

    public void callPullRequestRepo(Repository repository, final IRequestRepository.IRequestPullRepo iRequestPullRepo) {
        ICallRepositorys iCallRepositorys = BaseRequest.getInstanceRetrofit().create(ICallRepositorys.class);
        Call<List<PullRequest>> listCall = iCallRepositorys.getItemRepository(repository.getOwner().getName(), repository.getNameRepository());

        listCall.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(@NonNull Call<List<PullRequest>> call, @NonNull Response<List<PullRequest>> response) {
                iRequestPullRepo.listPullRepository(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<PullRequest>> call, @NonNull Throwable t) {
                iRequestPullRepo.listPullRepository(null);
            }
        });
    }
}