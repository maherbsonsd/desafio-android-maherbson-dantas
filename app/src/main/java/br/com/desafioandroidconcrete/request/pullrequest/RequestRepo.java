package br.com.desafioandroidconcrete.request.pullrequest;

import android.support.annotation.NonNull;
import android.util.Log;

import br.com.desafioandroidconcrete.interfaces.IRequestRepository;
import br.com.desafioandroidconcrete.services.ICallRepositorys;
import br.com.desafioandroidconcrete.model.response.RepositoryResponse;
import br.com.desafioandroidconcrete.request.BaseRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by maher on 19/01/2018.
 */

public class RequestRepo {

    public void callResquestRepo(final IRequestRepository.IRequestRepo iRequestRepository, int page) {

        ICallRepositorys iCallRepositorys = BaseRequest.getInstanceRetrofit().create(ICallRepositorys.class);
        Call<RepositoryResponse> listCall = iCallRepositorys.getRepository(page);

        listCall.enqueue(new Callback<RepositoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<RepositoryResponse> call, @NonNull Response<RepositoryResponse> response) {
                RepositoryResponse repositoryResponseList = response.body();
                if (repositoryResponseList != null && repositoryResponseList.getRepositoryList() != null) {
                    iRequestRepository.listRepository(repositoryResponseList.getRepositoryList());
                }
            }

            @Override
            public void onFailure(@NonNull Call<RepositoryResponse> call, @NonNull Throwable t) {
                Log.i("Throwable", t.getMessage());
                iRequestRepository.listRepository(null);
            }
        });
    }

}
