package br.com.desafioandroidconcrete;

import android.app.Application;
import android.content.Context;

/**
 * Created by maher on 20/01/2018.
 */

public class GlobalContext extends Application {

    private static Context mContext;

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return mContext;
    }

}
