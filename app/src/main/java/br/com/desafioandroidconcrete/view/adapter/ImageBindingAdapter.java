package br.com.desafioandroidconcrete.view.adapter;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import br.com.desafioandroidconcrete.R;

/**
 * Created by maher on 19/01/2018.
 */

public class ImageBindingAdapter {

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        if (!url.equals("")) {
            Picasso.with(imageView.getContext()).load(url).resize(200, 200).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.ic_person_black_24dp);
        }
    }
}
