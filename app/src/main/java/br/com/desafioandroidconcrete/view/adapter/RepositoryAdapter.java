package br.com.desafioandroidconcrete.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import br.com.desafioandroidconcrete.R;
import br.com.desafioandroidconcrete.databinding.CardviewRepositoryBinding;
import br.com.desafioandroidconcrete.databinding.LayoutProgressBinding;
import br.com.desafioandroidconcrete.interfaces.IRecyclerViewClick;
import br.com.desafioandroidconcrete.model.entity.Repository;
import br.com.desafioandroidconcrete.view.holder.ProgressHolder;
import br.com.desafioandroidconcrete.view.holder.ViewHolderRepository;

/**
 * Created by maher on 20/01/2018.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<Repository> repositoryList;
    private IRecyclerViewClick recyclerViewClick;
    private CardviewRepositoryBinding cardviewRepositoryBinding;
    private LayoutProgressBinding layoutProgressBinding;

    private final int VIEW_ITEM = 0;
    private final int VIEW_LOADING = 1;

    public RepositoryAdapter(Context context, List<Repository> repositoryList, IRecyclerViewClick recyclerViewClick) {
        this.context = context;
        this.repositoryList = repositoryList;
        this.recyclerViewClick = recyclerViewClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        cardviewRepositoryBinding = DataBindingUtil.inflate(layoutInflater, R.layout.cardview_repository, parent, false);

        if(viewType == VIEW_ITEM) {
            return new ViewHolderRepository(cardviewRepositoryBinding);
        } else if (viewType == VIEW_LOADING) {
            layoutProgressBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_progress, parent, false);
            return new ProgressHolder(layoutProgressBinding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolderRepository) {
            Repository repository = repositoryList.get(position);
            ((ViewHolderRepository)holder).bind(repository, recyclerViewClick);
        } else if(holder instanceof ProgressHolder) {
            ((ProgressHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return repositoryList.size();
    }

}
