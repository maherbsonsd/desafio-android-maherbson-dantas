package br.com.desafioandroidconcrete.view.holder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.desafioandroidconcrete.databinding.CardviewPullRequestBinding;
import br.com.desafioandroidconcrete.interfaces.IRecyclerViewClick;
import br.com.desafioandroidconcrete.model.entity.PullRequest;

/**
 * Created by maher on 20/01/2018.
 */

public class ViewHolderPullRequest extends RecyclerView.ViewHolder {

    private CardviewPullRequestBinding cardviewPullRequestBinding;

    public ViewHolderPullRequest(CardviewPullRequestBinding itemView) {
        super(itemView.getRoot());
        cardviewPullRequestBinding = DataBindingUtil.bind(itemView.getRoot());
    }

    public void bind(final PullRequest pullRequest, final IRecyclerViewClick iRecyclerViewClick) {
        cardviewPullRequestBinding.setPullRequest(pullRequest);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRecyclerViewClick.object(pullRequest);
            }
        });
    }
}
