package br.com.desafioandroidconcrete.view.holder;

import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import br.com.desafioandroidconcrete.databinding.CardviewRepositoryBinding;
import br.com.desafioandroidconcrete.databinding.LayoutProgressBinding;

/**
 * Created by maher on 20/01/2018.
 */

public class ProgressHolder extends RecyclerView.ViewHolder {
    public ProgressBar progressBar;
    public LayoutProgressBinding layoutProgressBinding;
    private CardviewRepositoryBinding cardviewRepositoryBinding;

    public ProgressHolder(LayoutProgressBinding itemView) {
        super(itemView.getRoot());
    }
}
