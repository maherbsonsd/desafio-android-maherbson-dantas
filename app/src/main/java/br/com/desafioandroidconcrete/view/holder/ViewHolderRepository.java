package br.com.desafioandroidconcrete.view.holder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.desafioandroidconcrete.databinding.CardviewRepositoryBinding;
import br.com.desafioandroidconcrete.interfaces.IRecyclerViewClick;
import br.com.desafioandroidconcrete.model.entity.Repository;

/**
 * Created by maher on 19/01/2018.
 */

public class ViewHolderRepository extends RecyclerView.ViewHolder {

    private CardviewRepositoryBinding cardviewRepositoryBinding;

    public ViewHolderRepository(CardviewRepositoryBinding itemView) {
        super(itemView.getRoot());
        cardviewRepositoryBinding = DataBindingUtil.bind(itemView.getRoot());
    }

    public void bind(final Repository repository, final IRecyclerViewClick recyclerViewClick) {
        cardviewRepositoryBinding.setRepository(repository);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewClick.object(repository);
            }
        });
    }

}