package br.com.desafioandroidconcrete.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import br.com.desafioandroidconcrete.R;
import br.com.desafioandroidconcrete.databinding.CardviewPullRequestBinding;
import br.com.desafioandroidconcrete.interfaces.IRecyclerViewClick;
import br.com.desafioandroidconcrete.model.entity.PullRequest;
import br.com.desafioandroidconcrete.view.holder.ViewHolderPullRequest;

/**
 * Created by maher on 20/01/2018.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<ViewHolderPullRequest> {

    private Context context;
    private List<PullRequest> lPullRequest;
    private IRecyclerViewClick iRecyclerViewClick;
    private CardviewPullRequestBinding cardviewPullRequestBinding;

    public PullRequestAdapter(Context context, List<PullRequest> lPullRequest, IRecyclerViewClick iRecyclerViewClick) {
        this.context = context;
        this.lPullRequest = lPullRequest;
        this.iRecyclerViewClick = iRecyclerViewClick;
    }

    @Override
    public ViewHolderPullRequest onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        cardviewPullRequestBinding = DataBindingUtil.inflate(layoutInflater, R.layout.cardview_pull_request, parent, false);
        return new ViewHolderPullRequest(cardviewPullRequestBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolderPullRequest holder, int position) {
        PullRequest pullRequest = lPullRequest.get(position);
        holder.bind(pullRequest, iRecyclerViewClick);
    }

    @Override
    public int getItemCount() {
        return lPullRequest.size();
    }
}
