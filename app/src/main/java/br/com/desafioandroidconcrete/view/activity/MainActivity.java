package br.com.desafioandroidconcrete.view.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroidconcrete.R;
import br.com.desafioandroidconcrete.interfaces.IRequestRepository;
import br.com.desafioandroidconcrete.request.pullrequest.RequestRepo;
import br.com.desafioandroidconcrete.util.NetworkUtil;
import br.com.desafioandroidconcrete.view.adapter.RepositoryAdapter;
import br.com.desafioandroidconcrete.databinding.ActivityMainBinding;
import br.com.desafioandroidconcrete.interfaces.IRecyclerViewClick;
import br.com.desafioandroidconcrete.model.entity.Repository;

public class MainActivity extends AppCompatActivity {

    public static final String REPOSITORY = "PULL_REQUEST";

    private ActivityMainBinding activityMainBinding;
    private RepositoryAdapter repositoryAdapter;
    private List<Repository> lRepositoryList = null;
    private int page = 1;
    private boolean loadMore = true;
    private LinearLayoutManager linearLayoutManager;

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            lRepositoryList = savedInstanceState.getParcelableArrayList(REPOSITORY);
            configureView();
            loadAdapter(lRepositoryList);
        } else {
            configureView();
            callResquest(page);
        }

        activityMainBinding.rvRepository.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (loadMore) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            page = page + 1;
                            callResquest(page);
                            loadMore = false;
                        }
                    }
                }
            }
        });
    }

    private void configureView() {
        linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.rvRepository.setHasFixedSize(true);
        activityMainBinding.rvRepository.setLayoutManager(linearLayoutManager);
    }

    private void callResquest(int page) {
        if (NetworkUtil.checkInternet()) {
            RequestRepo requestRepo = new RequestRepo();
            requestRepo.callResquestRepo(new IRequestRepository.IRequestRepo() {
                @Override
                public void listRepository(List<Repository> repositoryList) {
                    if (repositoryList != null && repositoryList.size() > 0) {
                        if (lRepositoryList == null) {
                            lRepositoryList = repositoryList;
                            loadAdapter(repositoryList);
                            loadMore = true;
                        } else {
                            lRepositoryList.addAll(repositoryList);
                            repositoryAdapter.notifyItemRangeInserted((lRepositoryList.size() -  repositoryList.size()), lRepositoryList.size());
                            loadMore = true;
                        }
                    } else {
                        activityMainBinding.llEmptyRepository.setVisibility(View.VISIBLE);
                        activityMainBinding.progress.setVisibility(View.GONE);
                    }
                }
            }, page);
        } else {
            activityMainBinding.progress.setVisibility(View.GONE);
            activityMainBinding.llStatusInternet.setVisibility(View.VISIBLE);
        }
    }

    private void loadAdapter(List<Repository> lRepositoryList) {
        repositoryAdapter = new RepositoryAdapter(MainActivity.this, lRepositoryList, new IRecyclerViewClick() {
            @Override
            public void object(Object object) {
                Intent intent = new Intent(MainActivity.this, ActivityPullRequestRepository.class);
                intent.putExtra(ActivityPullRequestRepository.OBJECT_REPOSITORY, (Repository) object);
                startActivity(intent);
            }
        });
        activityMainBinding.rvRepository.setAdapter(repositoryAdapter);
        activityMainBinding.progress.setVisibility(View.GONE);

    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelableArrayList(REPOSITORY, (ArrayList<? extends Parcelable>) lRepositoryList);
    }
}
