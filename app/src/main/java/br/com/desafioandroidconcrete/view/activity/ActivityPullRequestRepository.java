package br.com.desafioandroidconcrete.view.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioandroidconcrete.R;
import br.com.desafioandroidconcrete.databinding.ActivityMainBinding;
import br.com.desafioandroidconcrete.interfaces.IRecyclerViewClick;
import br.com.desafioandroidconcrete.interfaces.IRequestRepository;
import br.com.desafioandroidconcrete.model.entity.PullRequest;
import br.com.desafioandroidconcrete.model.entity.Repository;
import br.com.desafioandroidconcrete.request.pullrequest.PullRequestRepo;
import br.com.desafioandroidconcrete.util.NetworkUtil;
import br.com.desafioandroidconcrete.view.adapter.PullRequestAdapter;

public class ActivityPullRequestRepository extends AppCompatActivity {

    public static final String OBJECT_REPOSITORY = "OBJECT_REPOSITORY";
    public static final String ARRAY_PULL_REQUEST = "PULL_REQUEST";
    private ActivityMainBinding activityMainBinding;
    private Repository repository;

    private PullRequestAdapter pullRequestAdapter;
    private List<PullRequest> lPullRequest = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            lPullRequest = savedInstanceState.getParcelableArrayList(ARRAY_PULL_REQUEST);
            configureView();
            loadAdapter(lPullRequest);
        } else {
            configureView();
            callResquest();
        }
    }

    private void configureView() {
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        activityMainBinding.rvRepository.setHasFixedSize(true);
        activityMainBinding.rvRepository.setLayoutManager(new LinearLayoutManager(ActivityPullRequestRepository.this));
    }

    private void callResquest() {
        if (getIntent().getExtras() != null) {
            repository = getIntent().getExtras().getParcelable(OBJECT_REPOSITORY);
            if (NetworkUtil.checkInternet()) {
                if (lPullRequest == null) {
                    PullRequestRepo pullRequestRepo = new PullRequestRepo();
                    pullRequestRepo.callPullRequestRepo(repository, new IRequestRepository.IRequestPullRepo() {
                        @Override
                        public void listPullRepository(List<PullRequest> pullRequestList) {
                            if (pullRequestList != null && pullRequestList.size() > 0) {
                                lPullRequest = pullRequestList;
                                loadAdapter(pullRequestList);
                            } else {
                                activityMainBinding.llEmptyRepository.setVisibility(View.VISIBLE);
                                activityMainBinding.progress.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            } else {
                activityMainBinding.progress.setVisibility(View.GONE);
                activityMainBinding.llStatusInternet.setVisibility(View.VISIBLE);
            }
        }
    }

    private void loadAdapter(List<PullRequest> pullRequestList) {
        pullRequestAdapter = new PullRequestAdapter(ActivityPullRequestRepository.this, pullRequestList, new IRecyclerViewClick() {
            @Override
            public void object(Object object) {
                PullRequest pullRequest = (PullRequest) object;
                Uri uri = Uri.parse(pullRequest.getHtml_url());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        activityMainBinding.rvRepository.setAdapter(pullRequestAdapter);
        activityMainBinding.progress.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelableArrayList(ARRAY_PULL_REQUEST, (ArrayList<? extends Parcelable>) lPullRequest);
    }
}
